@echo OFF
@echo 1 liste tous les arguments
@echo %*
@echo 2 etend %1 en supprimant les guillemets (")
@echo %~1 
@echo 3 etend %0 en nom de chemin d'acces reconnu        
@echo %~f0
@echo 4 etend %0 en lettre de lecteur uniquement
@echo %~d0
@echo 5 etend %0 en chemin d'acces uniquement
@echo %~p0
@echo 6 etend %0 en nom de fichier uniquement
@echo %~n0
@echo 7 etend %0 en extension de fichier uniquement
@echo %~x0
@echo 8 etend %0 en lettre de lecteur et chemin d'acces uniquement
@echo %~dp0
@echo 9 etend %0 en nom de fichier et extension uniquement
@echo %~nx0
@echo 10 chemin etendu contenant uniquement des noms courts
@echo %~s0
@echo 11 etend %0 en attributs du fichier
@echo %~a0
@echo 12 etend %0 en date/heure du fichier
@echo %~t0
@echo 13 etend %0 en taille du fichier
@echo %~z0 
@echo 14 etend %0 en DIR comme ligne en sortie
@echo %~ftza0
@echo 15 parcourt les repertoires de la variable d'environnement PATH et etend %0 en nom du premier fichier reconnu trouve. Si le nom de la variable d'environnement n'est pas defini ou que le fichier n'est pas trouve par la recherche, alors ce modificateur etend en chaine vide
@echo %~$PATH:0
@echo 16 parcourt les repertoires listes dans la variable d'environnement PATH a la recherche de %0 et etend en lettre de lecteur du premier trouve.
@echo %~dp$PATH:0
